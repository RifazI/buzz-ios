//
//  DefaultButtonStyle.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 7/19/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import SwiftUI

let defaultStyle = DefaultButtonStyle()
let smallStyle = SmallButtonStyle()

struct DefaultButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        buttonStyle(configuration, isLarge: true)
    }
}

struct SmallButtonStyle: ButtonStyle {
    
    func makeBody(configuration: Self.Configuration) -> some View {
        buttonStyle(configuration, isLarge: false)
    }
}

func circleOverlay() -> some View {
    return Circle()
        .foregroundColor(.foreground)
        .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/, x: 3, y: 3)
}

fileprivate func buttonStyle(_ configuration: DefaultButtonStyle.Configuration, isLarge: Bool) -> some View {
    var radius: CGFloat = 7
    var maxHeight: CGFloat = 40
    var maxWidth: CGFloat = 200
    if isLarge {
        radius = 10
        maxHeight = 50
        maxWidth = CGFloat.infinity
    }
    return configuration.label
        .frame(minWidth: 100, maxWidth: maxWidth, maxHeight: maxHeight)
        .foregroundColor(Color(UIColor.secondaryLabel))
        .background(
            ZStack {
                RoundedRectangle(cornerRadius: 10, style: .continuous)
            }
        )
        .shadow(radius: radius, x: 3, y: 3)
        .scaleEffect(configuration.isPressed ? 0.95: 1)
        .foregroundColor(.foreground)
        .animation(.spring())
        .padding(15)
}

struct DefaultButtonStyle_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            Button(action: {
                
            }) {
                Text("Default")
            }
            .buttonStyle(defaultStyle)
            Button(action: {
                
            }) {
                Text("Small")
            }
            .buttonStyle(smallStyle)
        }
    }
}
