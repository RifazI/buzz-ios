//
//  ImageViewList.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 2/17/21.
//  Copyright © 2021 Rifaz Iqbal. All rights reserved.
//

import SwiftUI

struct ImageViewList: View {
    var body: some View {
        GeometryReader { geo in
            ZStack {
                let images = StepImage.allCases
                ScrollView(.horizontal, content: {
                    HStack(alignment: .center) {
                        ForEach(images, id:\.self) { image in
                            ImageInfoView(image: image)
                        }.frame(width: (geo.size.width/1.1), height: (geo.size.width/1.1))
                    }
                })
            }
        }
    }
}

struct ImageViewList_Previews: PreviewProvider {
    static var previews: some View {
        ImageViewList()
    }
}

