//
//  BorderedViewModifier.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 7/19/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import SwiftUI

struct BorderedViewModifier: ViewModifier {
    
    var borderWidth: CGFloat = 0
    var borderRadius: CGFloat = 0
    
    init(borderWidth: CGFloat,
         borderRadius: CGFloat) {
        
        self.borderWidth = borderWidth
        self.borderRadius = borderRadius
    }
    
    func body(content: Content) -> some View {
        content
            .padding()
            .overlay(RoundedRectangle(cornerRadius: borderRadius)
                        .stroke(lineWidth: borderWidth)
                        .fill(Color.gray)
            )
    }
}

extension View {
    func bordered(borderWidth: CGFloat = 1, borderRadius: CGFloat = 8) -> some View {
        ModifiedContent(
            content: self,
            modifier: BorderedViewModifier(
                borderWidth: borderWidth,
                borderRadius: borderRadius
            )
        )
    }
}

struct BorderedViewModifier_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
