//
//  ViewExtensions.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 10/7/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import SwiftUI

extension View {
    @ViewBuilder func isHidden(_ shouldHide: Bool) -> some View {
        switch shouldHide {
        case true: self.hidden()
        case false: self
        }
    }
}
