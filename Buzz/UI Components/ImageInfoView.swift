//
//  ImageInfoView.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 2/17/21.
//  Copyright © 2021 Rifaz Iqbal. All rights reserved.
//

import SwiftUI

enum StepImage: String, CaseIterable {
    case stepOne = "stepOne"
    case stepTwo = "stepTwo"
    case stepThree = "stepThree"
}

enum StepText: String, CaseIterable {
    case textOne = "Insert battery into case"
    case textTwo = "Wait for yellow light to light up"
    case textThree = "When the battery connects you will be able to test it and hear the buzzer"
}

struct ImageInfoView: View {
    
    @State var image: StepImage
    
    var body: some View {
        GeometryReader { geo in
            Image(image.rawValue)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .overlay(OverlayView(name:StepText.textOne.rawValue))
                .cornerRadius(10)
                .padding(20)
                .frame(width: geo.size.width, height: geo.size.width)
                .shadow(radius: /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/, x: 3, y: 3)
        }
    }
}

struct OverlayView: View {
    
    let name: String
    let colors = [Color.gray.opacity(1), Color.gray.opacity(0)]
    var gradient: LinearGradient {
        LinearGradient(gradient: Gradient(colors: colors), startPoint: .bottom, endPoint: .center)
    }
    
    var body: some View {
        ZStack(alignment: .bottomLeading) {
            VStack {
                Rectangle().fill(Color.clear)
                Rectangle().fill(gradient)
            }
            Text(name).padding(5)
        }
        .foregroundColor(.white)
    }
}

struct ImageInfoView_Previews: PreviewProvider {
    static var previews: some View {
        ImageInfoView(image: StepImage.stepOne)
    }
}
