//
//  ColorExtension.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 1/5/21.
//  Copyright © 2021 Rifaz Iqbal. All rights reserved.
//

import SwiftUI

extension Color {
    static var background: Color {
        return Color("color_background")
    }
    
    static var foreground: Color {
        return Color("color_foreground")
    }
}
