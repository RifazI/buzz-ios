//
//  Icon.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 8/17/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import SwiftUI
import Combine

enum IconType: String, CaseIterable {
    case battery = "battery"
    case calculator = "calculator"
    case camera = "camera"
    case controller = "controller"
    case game = "game"
    case headphone = "headphone"
    case ipod = "ipod"
    case mouse = "mouse"
    case radio = "radio"
    case remote = "remote"
}

struct IconImage: View {
    
    @Environment(\.colorScheme) var colorScheme
    @Binding var battery: Battery?
    @State var isOverlay: Bool = true
    
    var body: some View {
        VStack {
            Image(battery?.icon ?? IconType.battery.rawValue)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .background(isOverlay ? circleOverlay() : nil, alignment: .bottomLeading)
        }
    }
}

struct IconImage_Previews: PreviewProvider {    @State static var battery: Battery?
    static var previews: some View {
        IconImage(battery: $battery)
    }
}
