//
//  BuyButton.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 2/10/21.
//  Copyright © 2021 Rifaz Iqbal. All rights reserved.
//

import SwiftUI

func BuyButton() -> some View {
    let buyURL = URL(string: "http://www.google.com")!
    return Button(action: {
        UIApplication.shared.open(buyURL)
    }) {
        Text("BUY")
    }
    .buttonStyle(smallStyle)
}
