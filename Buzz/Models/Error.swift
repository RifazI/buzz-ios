//
//  Error.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 8/1/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import Foundation

enum BuzzError: Error {
    case encoding
    case getError
    case parseError
}
