//
//  User.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 8/1/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift
import CodableFirebase

struct User: Codable, Identifiable {
    
    static var collection = "users"
    
    var id: String?
    var firstName: String?
    var lastName: String?
    var phone: String?
    var batteries: [String]?
    var createdTime: Timestamp?
    
    func create(completion: @escaping (Error?) -> Void) {
        do {
            var user = self
            user.createdTime = Timestamp(date: Date())
            if let data = try FirebaseEncoder().encode(user) as? [String : Any] {
                API.db().collection(User.collection).addDocument(data: data) { error in
                    completion(error)
                }
            }
        }
        
        catch {
            completion(BuzzError.parseError)
        }
    }
    
    static func get(completion: @escaping ([User]?, Error?) -> Void) {
        API.db().collection(collection).addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                completion(nil, BuzzError.getError)
                return
            }
            
            let users = documents.compactMap { queryDocumentSnapshot -> User? in
                var user: User?
                do {
                    user = try queryDocumentSnapshot.data(as: User.self)
                } catch {
                    completion(nil, BuzzError.parseError)
                }
                return user
            }
            
            completion(users, nil)
        }
    }
}
