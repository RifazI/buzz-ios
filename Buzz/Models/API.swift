//
//  API.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 8/1/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift


class API: NSObject {
    private static var privateDb: Firestore?
    
    static func db() -> Firestore {
        if privateDb == nil {
            let settings = FirestoreSettings()
            settings.isPersistenceEnabled = true
            
            privateDb = Firestore.firestore()
            privateDb!.settings = settings
        }
        
        return API.privateDb!
    }
}

extension Encodable {
    var dictionary: [String: Any]? {
        do {
            let data = try JSONEncoder().encode(self)
            return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
        }
        catch {
            print(error)
        }
        
        return nil
    }
}
