//
//  Battery.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 8/1/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import Foundation
import FirebaseFirestore
import FirebaseFirestoreSwift
import CodableFirebase

struct Battery: Codable, Identifiable, Hashable {
    
    static let collection = "batteries"
    
    var id: String?
    var uuid: String?
    var userId: String?
    var name: String?
    var icon: String?
    var version: String?
    var createdTime: Int64?
    var modifiedTime: Int64?
    
    private enum CodingKeys: String, CodingKey {
        case uuid, userId, icon, name, version, createdTime, modifiedTime
    }

    func delete(completion: @escaping (Error?) -> Void) {
        if id != nil {
            API.db().collection(Battery.collection).document(id!).delete() { error in
                completion(error)
            }
        }
    }
    
    func set(completion: @escaping (Battery, Error?) -> Void) {
        var battery = self
        
        let time = Int64(Date().timeIntervalSince1970*1000)
        if battery.id == nil {
            battery.id = UUID().uuidString
            battery.createdTime = time
        }
        
        else {
            battery.modifiedTime = time
        }
        let data = try! FirestoreEncoder().encode(battery)
        Firestore.firestore().collection(Battery.collection).document(battery.id!).setData(data) { error in
            completion(battery, error)
        }
    }
    
    static func get(completion: @escaping ([Battery]?, Error?) -> Void) {
        API.db().collection(collection).addSnapshotListener { querySnapshot, error in
            guard let documents = querySnapshot?.documents else {
                completion(nil, BuzzError.getError)
                return
            }
            
            let batteries = documents.compactMap { queryDocumentSnapshot -> Battery? in
                var battery: Battery?
                do {
                    battery = try queryDocumentSnapshot.data(as: Battery.self)
                    battery?.id = queryDocumentSnapshot.documentID
                } catch {
                    completion(nil, BuzzError.parseError)
                }
                return battery
            }
            
            completion(batteries, nil)
        }
    }
}
