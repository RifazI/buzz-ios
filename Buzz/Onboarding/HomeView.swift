//
//  IntroView.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 7/21/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import SwiftUI

struct IntroView: View {
    
    private var model = PulsatingViewModel()

    var body: some View {
        VStack {
            PulsatingView(viewModel: model)
            Button(action: {
                
            }) {
                Text("Add battery")
            }
            .buttonStyle(DefaultButtonStyle())
            Spacer()
        }.navigationBarBackButtonHidden(true)
        .navigationBarTitle("BUZZ")
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        IntroView()
    }
}
