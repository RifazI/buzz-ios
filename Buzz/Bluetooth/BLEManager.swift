//
//  BLEManager.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 7/24/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import UIKit
import CoreBluetooth

class BLEManager: NSObject, ObservableObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    static var shared: BLEManager!
    static var centralManager: CBCentralManager!
    
    @Published var connectedPeripherals = [CBPeripheral]()
    
    var configCharacteristic: CBCharacteristic!
    var dataCharacteristic: CBCharacteristic!
        
    func buzz(beep: Bool, deviceUUID: UUID) {
        toggleData(toggle: beep, deviceUUID: deviceUUID)
        toggleConfig(deviceUUID: deviceUUID)
    }
    
    private func toggleConfig(deviceUUID: UUID) {
        if let peripheral = connectedPeripherals.filter({$0.identifier == deviceUUID}).first {
            var parameter: [UInt8] = [0x01]
            let data = Data(bytes: &parameter, count: 1)
            if ((configCharacteristic) != nil) {
                peripheral.writeValue(data, for: configCharacteristic, type: .withResponse)
            }
        }
    }
    
    private func toggleData(toggle: Bool, deviceUUID: UUID) {
        if let peripheral = connectedPeripherals.filter({$0.identifier == deviceUUID}).first {
            var parameter: [UInt8] = toggle ? [0x04]: [0x00]
            let data = Data(bytes: &parameter, count: 1)
            if ((dataCharacteristic) != nil) {
                peripheral.writeValue(data, for: dataCharacteristic, type: .withResponse)
            }
        }
    }
    
    func resetPeripherals() {
        for peripheral in connectedPeripherals {
            BLEManager.centralManager.cancelPeripheralConnection(peripheral)
        }
        connectedPeripherals = []
    }
    
    func stopScan() {
        BLEManager.centralManager.stopScan()
        
        for peripheral in connectedPeripherals {
            BLEManager.centralManager.cancelPeripheralConnection(peripheral)
        }
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state == CBManagerState.poweredOn {
            // Scan for peripherals if BLE is turned on
            central.scanForPeripherals(withServices: nil, options: nil)
        }
        else {
            //TODO: Show error
        }
    }
    
    // Check out the discovered peripherals to find Sensor Tag
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print(advertisementData)
        if SensorTag.sensorTagFound(advertisementData: advertisementData) == true {
            connectedPeripherals.append(peripheral)
            peripheral.delegate = self
            BLEManager.centralManager.connect(peripheral, options: nil)
        }
        else {
            //TODO:
            //self.statusLabel.text = "Sensor Tag NOT Found"
        }
    }
    
    // If disconnected, start searching again
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        let uuid = peripheral.identifier
        connectedPeripherals = connectedPeripherals.filter({$0.identifier != uuid})
    }
    
    // Discover services of the peripheral
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        connectedPeripherals.append(peripheral)
        peripheral.discoverServices(nil)
    }
    
    //Delegate methods
    
    // Check if the service discovered is valid i.e. one of the following:
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service in peripheral.services! {
            let thisService = service as CBService
            if SensorTag.validService(service: thisService) {
                // Discover characteristics of all valid services
                peripheral.discoverCharacteristics(nil, for: thisService)
            }
        }
    }
    
    // Enable notification and sensor for each characteristic of valid service
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        for characteristic in service.characteristics! {
            if (characteristic.uuid == IOConfigUUID) {
                configCharacteristic = characteristic
            }
            
            else if (characteristic.uuid == IODataUUID) {
                dataCharacteristic = characteristic
            }
        }
    }
    
    // Get data values when they are updated
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if (characteristic.uuid == IOConfigUUID) {
            //            configCharacteristic = characteristic
        }
        
        else if (characteristic.uuid == IODataUUID) {
            //            dataCharacteristic = characteristic
        }
    }
}
