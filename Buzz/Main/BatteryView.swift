//
//  BatteryView.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 8/6/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import SwiftUI

struct BatteryView: View {
    
    static let defaultName = "Battery"
    
    @State var battery: Battery!
    @State var isPeripheralSearching = true
    @State var isBuzz = false
    @State var isShowingAlert = false
    @State var name = ""
    @State var isShowIcons = false
    @State var isHideBackButton = false
    
    var body: some View {
        ZStack {
            Color.background.edgesIgnoringSafeArea(.all)
            VStack {
                NavigationLink("", destination: IconsView(battery: $battery), isActive: $isShowIcons)
                Spacer()
                IconImage(battery: $battery)
                    .onTapGesture {
                        isShowIcons = true
                    }
                    .frame(width: 200)
                    .padding(30)
                TextField("Name battery",
                          text: $name,
                          onEditingChanged: { edit in
                            print(edit)
                          },
                          onCommit: {
                            update()
                          }
                )
                .bordered()
                .padding(15)
                Spacer()
                ActivityIndicator(isAnimating: $isPeripheralSearching, style: .medium)
                Spacer()
                Button(action: {
                    self.isBuzz = !self.isBuzz
                    
                    if self.isShowingAlert == false {
                        if isBuzz == false {
                            self.isShowingAlert = true
                        }
                    }
                    
                    let deviceUUID = UUID(uuidString: battery.uuid ?? "")
                    
                    BLEManager.shared.buzz(beep: isBuzz, deviceUUID: deviceUUID!)
                }) {
                    Text("BUZZ")
                }
                .buttonStyle(defaultStyle)
                .opacity(isPeripheralSearching ? 0 : 1)
                .navigationBarTitle(battery?.name ?? "Name the battery")
            }
            .onAppear() {
                name = battery?.name ?? BatteryView.defaultName
            }
            .onReceive(BLEManager.shared.$connectedPeripherals, perform: { peripherals in
                let deviceUUID = UUID(uuidString: battery.uuid ?? "")
                isPeripheralSearching = isDeviceFound(uuid: deviceUUID)
            })
            .navigationBarBackButtonHidden(isHideBackButton)
        }
    }
    
    private func isDeviceFound(uuid: UUID?) -> Bool {
        let peripherals = BLEManager.shared.connectedPeripherals.filter({$0.identifier == uuid})
        if peripherals.count > 0 {
            return false
        } else {
            return true
        }
    }
    
    private func update() {
        battery.name = name
        battery.set(completion: { battery, error in
            if error != nil {
                isShowingAlert = true
            }
            else {
                self.battery = battery
            }
        })
    }
    
    class BatteryIcon: ObservableObject {
        @Published var batteryIcon = IconType.battery
    }
}

struct BatteryView_Previews: PreviewProvider {
    static var previews: some View {
        BatteryView(battery: Battery())
    }
}
