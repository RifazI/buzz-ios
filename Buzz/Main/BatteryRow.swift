//
//  BatteryRow.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 8/8/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import SwiftUI
import CoreBluetooth

struct BatteryRow: View {
    @State var battery: Battery!
    @ObservedObject var bleManager = BLEManager.shared
    @State var isPeripheralSearching = true
    
    var body: some View {
        HStack {
            IconImage(battery: $battery, isOverlay: false)
                .frame(width: 70)
            VStack(alignment: .leading) {
                Text(battery.name ?? "")
            }
            Spacer()
            ActivityIndicator(isAnimating: $isPeripheralSearching, style: .medium)
        }.onReceive(BLEManager.shared.$connectedPeripherals, perform: { peripherals in
            isPeripheralSearching = isDeviceFound(uuid: UUID(uuidString: battery.uuid ?? ""))
        })
    }
    
    private func isDeviceFound(uuid: UUID?) -> Bool {
        let peripherals = BLEManager.shared.connectedPeripherals.filter({$0.identifier == uuid})
        if peripherals.count > 0 {
            return false
        } else {
            return true
        }
    }
}

struct DeviceRow_Previews: PreviewProvider {
    static var battery: Battery?
    static var previews: some View {
//        battery?.name = "TV Remote"
        BatteryRow(battery: Battery())
    }
}
