//
//  HomeView.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 8/2/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import SwiftUI
import FirebaseUI

struct HomeView: View {
    
    @State private var MainviewState = CGSize.zero
    @State private var isShowingSearchView = false
    @State private var isShowingErrorAlert = false
    @State private var isShowingDeleteAlert = false
    @State private var isShowingDeleteErrorAlert = false
    @State private var isShowingLogoutAlert = false
    @State private var isShowingSplash = false
    @State var batteries = [Battery]()
    @State private var deleteIndexSet: IndexSet?
    
    var searchView = SearchView()
    
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        ZStack {
            Color.background.edgesIgnoringSafeArea(.all)
            VStack {
                if batteries.count > 0 {
                    ZStack {
                        Color.background.edgesIgnoringSafeArea(.all)
                        List {
                            ForEach(batteries, id: \.self) { battery in
                                NavigationLink(destination: BatteryView(battery: battery)) {
                                    BatteryRow(battery: battery)
                                }
                            }.onDelete(perform: { indexSet in
                                self.isShowingDeleteAlert = true
                                self.deleteIndexSet = indexSet
                            }).alert(isPresented:$isShowingDeleteAlert) {
                                deleteAlert()
                            }.listRowBackground(Color.background)
                        }.listStyle(SidebarListStyle())
                        .onAppear{
                            initialize()
                        }
                    }
                }
                
                else {
                    Spacer()
                    Text("Add your first buzz battery!")
                    Image("battery")
                    .resizable()
                    .scaledToFit()
                    .padding(100)
                }
                BuyButton()
                Spacer()
                SearchButton()
                .sheet(isPresented: $isShowingSearchView) {
                    searchView
                }
                .alert(isPresented:$isShowingErrorAlert) {
                    errorAlert()
                }
            }
            .navigationBarBackButtonHidden(true)
            .navigationBarTitle("BUZZ")
            .navigationBarItems(trailing: logoutButton())
            .onAppear {
                self.getBatteries()
            }
            //Need this for when you come back to this view
            .onDisappear {
                self.getBatteries()
            }
            .alert(isPresented:$isShowingDeleteErrorAlert) {
                deleteErrorAlert()
            }
        }
    }
    
    private func initialize() {
        UITableView.appearance().backgroundColor = .clear
        UITableViewCell.appearance().backgroundColor = .clear
        UITableView.appearance().tableFooterView = UIView()
     }
    
    fileprivate func logoutButton() -> some View {
        return Button(action: {
            isShowingLogoutAlert = true
        }) {
            Image(systemName: "person.crop.circle").imageScale(.large)
        }.alert(isPresented:$isShowingLogoutAlert) {
            logoutAlert()
        }
    }
    
    fileprivate func SearchButton() -> some View {
        return Button(action: {
            self.isShowingSearchView = true
        }) {
            Text("Activate a Buzz")
        }
        .buttonStyle(defaultStyle)
    }
    
    private func logout() {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            presentationMode.wrappedValue.dismiss()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    private func getBatteries() {
        Battery.get { batteries, error in
            if error == nil {
                self.batteries = batteries ?? []
            }
            
            else {
                isShowingErrorAlert = true
            }
        }
    }
    
    private func logoutAlert() -> Alert {
        return Alert(title: Text("Are you sure you want to logout?"),
                     message: Text(""),
                     primaryButton: .destructive(Text("Logout")) {
                        logout()
                     },
                     secondaryButton: .cancel())
    }
    
    private func errorAlert() -> Alert {
        return Alert(title: Text("Could get batteries"), message: Text("Please check your internet and try again"), dismissButton: .default(Text("OK")))
    }
    
    private func deleteErrorAlert() -> Alert {
        return Alert(title: Text("Could delete battery"), message: Text("Please check your internet and try again"), dismissButton: .default(Text("OK")))
    }
    
    private func deleteAlert() -> Alert {
        return Alert(title: Text("Are you sure you want to delete?"),
                     message: Text(""),
                     primaryButton: .destructive(Text("Delete")) {
                        delete(deleteIndexSet!)
                     },
                     secondaryButton: .cancel())
    }
    
    fileprivate func delete(_ atOffsets: IndexSet) {
        atOffsets.forEach { index in
            batteries[index].delete { error in
                if error != nil {
                    isShowingDeleteErrorAlert = true
                }
            }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
        HomeView(batteries: [Battery()])
    }
}
