//
//  DeviceNameView.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 7/29/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import SwiftUI

struct DeviceNameView: View {
    
    var uuid: UUID!
    
    init(uuid: UUID) {
        self.uuid = uuid
    }
    
    @State var name = ""
    @State var isShowHome = false
    @State var isShowingAlert = false
    
    var homeView = HomeView()
    
    var body: some View {
        VStack {
            NavigationLink("", destination: homeView, isActive: $isShowHome)
            Spacer()
            Text("Name your Buzz device")
            TextField("Name", text: $name)
                .bordered()
                .padding(15)
            Button(action: {
                addDevice(uuid: uuid)
            }) {
                Text("DONE")
            }
            .buttonStyle(DefaultButtonStyle())
            .alert(isPresented:$isShowingAlert) {
                Alert(title: Text("Could not add buzzer"), message: Text("Please check your internet and try again"), dismissButton: .default(Text("OK")))
            }
            Spacer()
        }
    }
    
    private func addDevice(uuid: UUID) {
        var battery = Battery()
        battery.uuid = uuid.uuidString
        battery.name = name
        battery.add(completion: { error in
            if error == nil {
                isShowHome = true
            }
            
            else {
                isShowingAlert = true
            }
        })
    }
}

struct AddDeviceView_Previews: PreviewProvider {
    static var previews: some View {
        DeviceNameView(uuid: UUID())
    }
}
