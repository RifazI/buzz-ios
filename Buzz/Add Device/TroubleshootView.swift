//
//  TroubleshootView.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 7/26/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import SwiftUI

struct TroubleshootView: View {
    @Environment(\.presentationMode) var presentationMode

    var body: some View {
        ZStack {
            Color.background.edgesIgnoringSafeArea(.all)
            Text("1. Make sure the Buzz device is close to your phone\n" +
                    "2. Remove AA battery from the Buzz device\n" +
                    "3. Restart the bluetooth on your phone\n" +
                    "4. Put the AA battery back into device")
        }
    }
}
