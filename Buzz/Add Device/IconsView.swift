//
//  IconsView.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 8/18/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import SwiftUI

struct IconsView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    @Binding var battery: Battery!
    
    var body: some View {
        ZStack {
            Color.background.edgesIgnoringSafeArea(.all)
            let icons = IconType.allCases.chunked(into: 2)
            
            List {
                ForEach(0..<icons.count) { index in
                    HStack {
                        ForEach(icons[index], id:\.self) { icon in
                            Image(icon.rawValue)
                                .resizable()
                                .padding(30)
                                .scaledToFit()
                                .onTapGesture {
                                    setIcon(icon: icon)
                                }
                        }
                    }
                }.listRowBackground(Color.background)
            }.padding(.leading, -20)
            .padding(.trailing, -20)
            .listStyle(SidebarListStyle())
            .onAppear{
                initialize()
            }.navigationBarTitle(Text("Pick a new icon"))
        }
    }
    
    private func initialize() {
        UITableView.appearance().backgroundColor = .clear
        UITableViewCell.appearance().backgroundColor = .clear
        UITableView.appearance().tableFooterView = UIView()
     }
    
    private func setIcon(icon: IconType) {
        battery.icon = icon.rawValue

        battery.set { battery, error in
            if error == nil {
                self.battery = battery
                self.presentationMode.wrappedValue.dismiss()
            }
        }
    }
}

private extension Array {
    func chunked(into size:Int) -> [[Element]] {
        
        var chunkedArray = [[Element]]()
        
        for index in 0...self.count {
            if index % size == 0 && index != 0 {
                chunkedArray.append(Array(self[(index - size)..<index]))
            } else if(index == self.count) {
                chunkedArray.append(Array(self[index - 1..<index]))
            }
        }
        
        return chunkedArray
    }
}

struct IconPickerView_Previews: PreviewProvider {
    @State static var battery: Battery?
    static var previews: some View {
        IconsView(battery: $battery)
//        IconPickerView(, battery: Binding<Battery?>)
    }
}
