//
//  SearchView.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 7/24/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import SwiftUI
import CoreBluetooth

struct SearchView: View {
    @Environment(\.presentationMode) var presentationMode
    
    private var pulseView = PulsatingViewModel()
    
    @ObservedObject var bleManager = BLEManager.shared
    @State private var isHideTroubleshootButton = false
    @State private var isShowTroubleshoot = false
    @State private var batteries = [Battery]()
    @State private var isTestHidden = true
    @State private var uuid = UUID()
    @State private var isBuzz = false
    @State private var isShowAddScreen = false
    @State private var isShowConfirmAlert = false
    @State private var isShowSaveAlert = false

    @State var battery = Battery()

    @State private var newPeripheral: CBPeripheral! {
        didSet {
            uuid = newPeripheral.identifier
        }
    }
    
    var body: some View {
        ZStack {
            Color.background.edgesIgnoringSafeArea(.all)
            VStack {
                NavigationLink("", destination: BatteryView(battery: battery, isHideBackButton: true), isActive: $isShowAddScreen)
                Spacer()
                ImageViewList()
                Spacer()
                Button(action: {
                    isShowTroubleshoot = true
                }) {
                    Text("Can't find it?")
                }.sheet(isPresented: self.$isShowTroubleshoot) {
                    TroubleshootView()
                }
                .buttonStyle(smallStyle)
                ActivityIndicator(isAnimating: $isTestHidden, style: .medium)
                Spacer()
                Button(action: {
                    self.isBuzz = !self.isBuzz
                    
                    if self.isShowConfirmAlert == false {
                        if isBuzz == false {
                            self.isShowConfirmAlert = true
                        }
                    }
                    
                    BLEManager.shared.buzz(beep: isBuzz, deviceUUID: uuid)
                }) {
                    Text("Test buzzer")
                }
                .buttonStyle(defaultStyle)
                .isHidden(isTestHidden)
                .alert(isPresented:$isShowConfirmAlert) {
                    Alert(title: Text("Did you hear the buzzer?"), message: Text(""), primaryButton: .default(Text("Yes")) {
                        update()
                    }, secondaryButton: .destructive(Text("No")))
                }
            }

        }.navigationBarTitle("Searching", displayMode: .inline)
        .onReceive(BLEManager.shared.$connectedPeripherals, perform: { peripherals in
            getBatteries()
        })
        .onAppear {
            getBatteries()
        }
    }
    
    private func update() {
        battery.name = "Battery"
        battery.uuid = uuid.uuidString
        battery.set { battery, error in
            if error == nil {
                self.battery = battery
                isShowAddScreen = true
            }
            else {
                //TODO: Show error
            }
        }
    }
    
    private func undiscovered() {
        var undiscovered = [CBPeripheral]()
        let uuids = batteries.map{$0.uuid}
        
        for peripheral in bleManager.connectedPeripherals {
            if !(uuids.contains(peripheral.identifier.uuidString)) {
                undiscovered.append(peripheral)
            }
        }
        
        if undiscovered.count > 0 {
            newPeripheral = undiscovered.first
            isTestHidden = false
        }
    }
    
    private func delayText() {
        // Delay of 20 seconds
        DispatchQueue.main.asyncAfter(deadline: .now() + 20) {
            self.isHideTroubleshootButton = true
        }
    }
    
    private func getBatteries() {
        if batteries.count == 0 {
            Battery.get { batteries, error in
                if error == nil {
                    self.batteries = batteries ?? []
                    undiscovered()
                }
            }
        }
        
        else {
            undiscovered()
        }
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
    }
}
