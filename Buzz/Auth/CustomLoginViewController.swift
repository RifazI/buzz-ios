//
//  CustomLoginViewController.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 7/20/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import SwiftUI
import FirebaseUI
import Firebase

struct CustomLoginViewController : UIViewControllerRepresentable {
    
    //Only way callback works. Strong pointer I guess
    static var coordinator: Coordinator!
    
    var dismiss : (_ error : Error? ) -> Void
    
    func makeCoordinator() -> CustomLoginViewController.Coordinator {
        Coordinator(self)
    }
    
    func makeUIViewController(context: Context) -> UIViewController {
        let authUI = FUIAuth.defaultAuthUI()
        
        let providers : [FUIAuthProvider] = [
            FUIEmailAuth(),
            FUIOAuth.appleAuthProvider(),
            FUIGoogleAuth()
        ]
        
        authUI?.providers = providers
        authUI?.delegate = context.coordinator
        CustomLoginViewController.coordinator = context.coordinator
        
        let authViewController = authUI?.authViewController()
        
        return authViewController!
    }
    
    func updateUIViewController(_ uiViewController: UIViewController, context: UIViewControllerRepresentableContext<CustomLoginViewController>) {
        //        dismiss(nil)
    }
    
    //coordinator
    class Coordinator : NSObject, FUIAuthDelegate {
        var parent : CustomLoginViewController
        
        init(_ customLoginViewController : CustomLoginViewController) {
            self.parent = customLoginViewController
        }
        
        // MARK: FUIAuthDelegate
        
        func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
            if let error = error {
                parent.dismiss(error)
            }
            else {
                parent.dismiss(nil)
            }
        }
    }
}

struct CustomLoginViewController_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
