//
//  LoginView.swift
//
//
//  Created by Rifaz Iqbal on 7/19/20.
//

import SwiftUI
import FirebaseAuth

public var screenHeight: CGFloat {
    return UIScreen.main.bounds.height
}

struct LoginView: View {
    
    @State private var viewState = CGSize(width: 0, height: screenHeight)
    @State private var MainviewState = CGSize.zero
    @State var isLoginShown = false
    @State var isHomeShown = false
    
    var homeView = HomeView()
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.background.edgesIgnoringSafeArea(.all)
                VStack {
                    NavigationLink("", destination: homeView, isActive: $isHomeShown)
                    Spacer()
                    Button(action: {
                        self.checkUserAvailable()
                    }) {
                        Text("LOGIN")
                    }
                    .buttonStyle(defaultStyle)
                }
            }
        }
        .sheet(isPresented: $isLoginShown) {
            CustomLoginViewController { error in
                if error == nil {
                    isHomeShown = true
                    self.status()
                }
            }.offset(y: self.MainviewState.height).animation(.spring())
        }.onAppear{
            self.checkUserAvailable()
        }
    }
    
    func status() {
        self.viewState = CGSize(width: 0, height: 0)
        self.MainviewState = CGSize(width: 0, height: screenHeight)
    }
    
    func checkUserAvailable() {
        if Auth.auth().currentUser == nil {
            isLoginShown = true
        }
        
        else {
            isHomeShown = true
        }
    }
}

struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
