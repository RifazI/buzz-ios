//
//  ContentView.swift
//  Buzz
//
//  Created by Rifaz Iqbal on 7/18/20.
//  Copyright © 2020 Rifaz Iqbal. All rights reserved.
//

import SwiftUI

struct ContentView : View {
    
    var body: some View {
        LoginView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
